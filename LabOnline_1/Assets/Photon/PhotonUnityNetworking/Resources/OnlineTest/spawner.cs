﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {
    public float spawnrate;
    public float spawn;
    public GameObject scoreObj;
    public GameObject spawnPoint;
    public float xrand;
    public float yrand;
    public float scoreCount;
    float count;
    public List<GameObject> scoreList = new List<GameObject>();
    public  static spawner Instance;
	// Use this for initialization
	void Start () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        
        spawn++;
        if (spawn>spawnrate&&scoreList.Count<scoreCount)
        {
            spawnPoint.transform.position = new Vector2(Random.Range(xrand, -xrand), Random.Range(yrand, -yrand));
            GameObject score = Instantiate(scoreObj, spawnPoint.transform.position, Quaternion.identity);
            spawn = 0;
            scoreList.Add(score);
        }
        Debug.Log("score :"+scoreList.Count);
	}
}
