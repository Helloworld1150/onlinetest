﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviourPunCallbacks
{
    float x;
    float y;
    float scaleX;
    float scaleY;
    public float scaleRate;
    public float speed;
    public float sizeCost;
    public static player Instance;
    public GameObject collisionObj;
    // Use this for initialization
    private void Awake()
    {
        PhotonTransformView Ptf = this.GetComponent<PhotonTransformView>();
        Ptf.m_SynchronizeScale = true;
    }
    void Start () {
        PhotonTransformView Ptf = this.GetComponent<PhotonTransformView>();
        Ptf.m_SynchronizeScale = true;
        Instance = this;
        x = 0;
        y = 0;
        scaleX = 5;
        scaleY = 5;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (photonView.IsMine)
        {
            PhotonTransformView Ptf = this.GetComponent<PhotonTransformView>();
            Ptf.m_SynchronizeScale = true;

            transform.Translate(new Vector2(x, y));
            if (Input.GetKey(KeyCode.W))
            {
                y += speed;
            }
            if (Input.GetKey(KeyCode.S))
            {
                y -= speed;
            }
            if (Input.GetKey(KeyCode.A))
            {
                x -= speed;
            }
            if (Input.GetKey(KeyCode.D))
            {
                x += speed;
            }
            transform.localScale = new Vector3(scaleX, scaleY, 1);
        }
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("hit");
        if (photonView.IsMine)
        {
            if (collision.tag == "score")
            {
                Destroy(collision.gameObject);
                //spawner.Instance.scoreList.Remove(collision.gameObject);
                sizeCost += 1;
                scaleX += scaleRate;
                scaleY += scaleRate;
            }
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collisionObj = collision.gameObject;
            PhotonView id = collision.gameObject.GetComponent<PhotonView>();
            Debug.Log("ID" + id.ViewID);
            Debug.Log("Bullet Collision to other player.");
            if (photonView != null)
                photonView.RPC("Destory", RpcTarget.All);
            player tempHealthOther = collision.gameObject.GetComponent<player>();
            if (tempHealthOther != null)
                tempHealthOther.Destory();
        }
        
    }
    public void Destory()
    {
        PunUserNetControlPlayer.Instance.ChangeColorProperties();
        //PhotonNetwork.Destroy(collisionObj);
    }
}
