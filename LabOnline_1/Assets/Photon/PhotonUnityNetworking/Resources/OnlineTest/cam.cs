﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cam : MonoBehaviour {
    public float z;
    public static cam Instance;
    public float maxsize;
    public Camera camObj;
    public float size;
	// Use this for initialization
	void Start () {
        Instance = this;
        z = -3;
        camObj = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        GameObject players = GameObject.FindGameObjectWithTag("Player");
        camObj.orthographicSize = size;
        if (player.Instance.sizeCost>maxsize)
        {
            size += 1.25f;
            maxsize += 7;
        }
        transform.position = new Vector3(players.transform.position.x, players.transform.position.y, z);
	}
}
