﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    public int healingPoint = 1;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Test other : " + other.gameObject.name);
        if (other.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = other.gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}