﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunGameSetting : MonoBehaviour
{
    public const float MIN_SPAWN_TIME = 5.0f;
    public const float MAX_SPAWN_TIME = 10.0f;
    public const float PLAYER_RESPAWN_TIME = 4.0f;
    public const string PLAYER_LIVES = "PlayerLives"; public const int PLAYER_MAX_LIVES = 3;
    public const string PLAYER_READY = "IsPlayerReady"; public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";
    public const string PLAYER_COLOR = "PlayerColor";
    public const string CountdownStartTime = "GameTime"; public const string GAMEOVER = "IsGameOver";

    public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            case 0: return Color.red;
            case 1: return Color.green;
            case 2: return Color.blue;
            case 3: return Color.yellow;
            case 4: return Color.cyan;
            case 5: return Color.grey;
            case 6: return Color.magenta;
            case 7: return Color.white;
        }

        return Color.black;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
