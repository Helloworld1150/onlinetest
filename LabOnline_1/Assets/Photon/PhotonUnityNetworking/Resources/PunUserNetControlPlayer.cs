﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControlPlayer : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback , IOnEventCallback
{
    [Tooltip("The local player instance. Use this to know if the local player isrepresented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public GameObject Wan;
    public static PunUserNetControlPlayer Instance;
    private void Start()
    {
        Instance = this;
    }

    public void OnEvent(EventData photonEvent)
    {
       // Debug.Log(photonEvent.ToStringFull());

        byte eventCode = photonEvent.Code;

        if (eventCode == (byte)PunUserEvent.ChangeColorEv)
        {
            Debug.Log("Call PunUserEvent Resise Event");
            object[] data = (object[])photonEvent.CustomData;

            if (this.photonView.ViewID == (int)data[0]){
                Debug.Log("Data" + (float)data[1] + ":"
                    + (float)data[2] + ":"
                    + (float)data[3]);
                Color _newColor = new Color((float)data[1], (float)data[2], (float)data[3]); MeshRenderer _mesh = GetComponent<MeshRenderer>();
                _mesh.material.color = _newColor;
            }
        }
    

    }
    public enum PunUserEvent{
        ChangeColorEv=0
    };

public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
      

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation whenlevels are synchronized
        /*if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            GetComponent<MeshRenderer>().material.color = Color.blue;
            Wan.SetActive(false);
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;
        }*/


    }
    private void ChangeColorRaiseEvent()
    {
        float r, g, b; r = Random.Range(0f, 1f);
        g = Random.Range(0f, 1f);
        b = Random.Range(0f, 1f);
        //ArraycontainstheviewIDandthecoloroftheselectedunits
        object[] content = new object[] { this.photonView.ViewID, r, g, b };
        //YouwouldhavetosettheReceiverstoAllinordertoreceivethisevent
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions() {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache };
        SendOptions sendOptions = new SendOptions() {
            Reliability = true
        };
        PhotonNetwork.RaiseEvent((byte)PunUserEvent.ChangeColorEv, content, raiseEventOptions, sendOptions); }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);

    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
    }
    private void Awake()
    {
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
 
    }

    void Update() {
        if (!photonView.IsMine) return;

        if (Input.GetKeyDown(KeyCode.C))
        {
            ChangeColorProperties();
        }

    }
    public void ChangeColorProperties() {
        Hashtable props = new Hashtable
        {
            {
                PunGameSetting.PLAYER_COLOR, Random.Range(0, 7)
            }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }
    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) && target.ActorNumber == photonView.ControllerActorNr)
        { object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                GetComponent<SpriteRenderer>().material.color = PunGameSetting.GetColor((int)colors);
            } return;
        }
    }
}