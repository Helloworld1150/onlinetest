﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

public class PunNetworkManager : ConnectAndJoinRandom
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    bool GameStart = false;

    bool isFirstSetting = false;
    public GameObject HealingPrefab;
    public GameObject ScorePrefab;
    public int numberOfHealing = 5;
    float m_count = 0;
    public float m_CountDownDropHeal = 10;
    public GameObject spawnPlayer;
    public GameObject spawnPoint;
    public float xrand;
    public float yrand;
    private void Awake()
    {
        singleton = this;
    }

    private void Update()
    {

        if (PhotonNetwork.IsMasterClient != true)
            return;

        if (GameStart == true)
        {
            if (isFirstSetting == false)
            {

                isFirstSetting = true;

                int half = numberOfHealing / 2;
                for (int i = 0; i < half; i++)
                {

                    PhotonNetwork.InstantiateSceneObject(ScorePrefab.name, spawnPoint.transform.position, Quaternion.identity);
                    spawnPoint.transform.position = new Vector2(Random.Range(xrand, -xrand), Random.Range(yrand, -yrand));
                }
                m_count = m_CountDownDropHeal;
            }
            else
            {
                if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing)
                {
                    m_count -= Time.deltaTime;

                    if (m_count <= 0)
                    {
                        m_count = m_CountDownDropHeal;

                        PhotonNetwork.Instantiate(ScorePrefab.name, spawnPoint.transform.position, Quaternion.identity);
                        spawnPoint.transform.position = new Vector2(Random.Range(xrand, -xrand), Random.Range(yrand, -yrand));
                    }
                }
            }


        }
    }

    public Vector3 RandomPosition(float yOffset)
    {
        var spawnPosition = new Vector3(
            Random.Range(-30.0f, 30.0f),
            yOffset,
            Random.Range(-30.0f, 30.0f));
        return spawnPosition;
    }

    public Quaternion RandomRotation()
    {
        var spawnRotation = Quaternion.Euler(
            1.0f,
            Random.Range(0, 180),
            0.0f);

        return spawnRotation;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log("New Player. " + newPlayer.ToString());

    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        //Camera.main.gameObject.SetActive(true);
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, spawnPlayer.transform.position, Quaternion.identity, 0);

        GameStart = true;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        if (Camera.main != null)
            Camera.main.gameObject.SetActive(true);
    }
}
